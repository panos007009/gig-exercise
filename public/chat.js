//Make connection
var socket = io.connect('http://localhost:4000')

//Query DOM
var message = document.getElementById('message');
var user = document.getElementById('user');
var btn = document.getElementById('send');
var output = document.getElementById('output');
var feedback = document.getElementById('feedback');

//Emit events

//Listener for clicking send button to send a message
btn.addEventListener('click', () => {
    socket.emit('chat', {
        message : message.value,
        user : user.value
    });
    //Reset message field value after sending the message
    message.value = null;
});

//Listener for pressing enter button to send a message
document.addEventListener('keypress', (e) => {
    if (e.key === 'Enter') {
        socket.emit('chat', {
            message : message.value,
            user : user.value
        });
        message.value = null;
    }
});

//Event for emitting "A user is typing..." message
message.addEventListener('keypress', () => {
    socket.emit('typing', user.value)
});


//Listen for events
socket.on('chat', (data) => {
    feedback.innerHTML = "";
    output.innerHTML += `<p><strong> ${data.user}</strong> : ${data.message}</p>`

});

socket.on('typing', (data) => {
    feedback.innerHTML = `<p><em> ${data} is typing a message...</em></p>`;
});