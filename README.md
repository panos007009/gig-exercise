# Exercise of the The Core-Tech Challenge of GiG

A dockerized, JavaScript(Node.js), live chat application, demonstrating usage of websockets utilizing express and socket.io.

## How to run the app

1. Open a terminal and navigate to the application folder
2. Type `docker build -t gigjob .` and the docker image will be created
3. Type `docker run -p 4000:4000 gigjob` and the container will start on port 4000

## How to use the app

Open your favourite browser and navigate to http://localhost:4000/. You can open as many instances of localhost in your browser to simulate different users connecting to the chat room. By typing you name and message, on the appropriate input boxes and pressing the "Send" button or enter, your selected username along with message will be displayed to all connected users.(open browser tabs connected to http://localhost:4000/)